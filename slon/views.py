from django.shortcuts import render
from .utils import translate2rend
from .forms import TransForm


def index(request):
	if request.method == "POST":
		form = TransForm(request.POST)
		if form.is_valid():
			text2translate = form.cleaned_data['text2translate']
			translated_texts = translate2rend(text2translate)

			context = {
					'res1': translated_texts[0],
					'res2': translated_texts[1],
					"form": form,
					}

			return render(request, 'slon/index.html', context)

	else:
		form = TransForm()
	return render(request, 'slon/index.html', {"form": form})
